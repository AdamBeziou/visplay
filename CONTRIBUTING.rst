Contributing Guidelines
=======================

Contributions are welcome! Please fork and submit a pull request. If you are a
member the Mines ACM Student Chapter, you should be able to work out of this
repository.  If you don't have access to this repository, just ask one of the
officers.

We need contributions in three major areas:

1. **Code.** See our `Development Quickstart Guide`_ for information on how to
   get started developing Visplay.
2. **Bug Reports.** Open an issue using the "Bug" template.
3. **Feature Requests.** Open an issue using the "Feature Request" template.

.. _Development Quickstart Guide: https://coloradoschoolofmines.gitlab.io/visplay/development-quickstart.html

Code Style
----------

- For Python code, follow `PEP-8`__. However, lines of up to 100 characters are
  acceptable if the line still clearly conveys the intent of the code.

- For documentation, only use reStructuredText. Lines should not exceed 80
  characters, except when there is a URL or other literal which needs to exceed
  this limit.

- JavaScript should be indented 2 spaces.

__ https://www.python.org/dev/peps/pep-0008/
