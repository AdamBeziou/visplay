#!/usr/bin/env bash

set -xe

dnf install -y inkscape

# Install Requirements
pip3 install -r requirements.txt
pip3 install .

