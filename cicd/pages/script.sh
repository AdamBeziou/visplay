#!/usr/bin/env bash

set -xe

# Render the diagrams
pushd docs/diagrams
make
popd

# Compile the Sphinx documenation
make html

# Required for GitLab pages
mv build/html public
