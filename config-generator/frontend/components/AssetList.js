import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import TableWithAddButton from './core/TableWithAddButton';

import PropTypes from 'prop-types';

const Asset = ({ asset }) =>
  <TableRow>
    <TableCell>{asset.name}</TableCell>
    <TableCell>{asset.uri}</TableCell>
  </TableRow>;
Asset.propTypes = {
  asset: PropTypes.object.isRequired,
};

/**
 * A list of all the assets.
 *
 * @class
 */
class AssetList extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    assets: PropTypes.array.isRequired,
    loading: PropTypes.bool.isRequired,
    on_addButton_click: PropTypes.func.isRequired,
  };

  render() {
    const { classes, assets, loading, on_addButton_click } = this.props;

    return (
      <TableWithAddButton className={classes.card} headerText="Assets" loading={loading}
        on_addButton_click={on_addButton_click.bind(this)}>
        <TableHead>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell>URI</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {assets.map((a, i) => <Asset key={i} asset={a} />)}
        </TableBody>
      </TableWithAddButton>
    );
  }
}

export default withStyles(() => {
  return {};
})(AssetList);
