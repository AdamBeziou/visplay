import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';

import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import AssetTab from './AssetTab';


const styles = theme => {
  return {
    root: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.paper,
    },
  };
};

/**
 * Main tab container
 */
class AppTabs extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
  }

  state = {
    tabValue: 0,

    // Assets
    assets: [],
    assetsLoading: true,

    // Playlists
    playlists: [],
    playlistsLoading: true,
  }


  componentDidMount() {
    this.setState({ assetsLoading: true, playlistsLoading: true });
    axios.get('/assets')
      .then(res =>
        this.setState({ assets: res.data, assetsLoading: false })
      );
  }

  on_TabChange(event, tabValue) {
    this.setState({ tabValue });
  }

  render() {
    const { classes } = this.props;
    const { assets, assetsLoading, tabValue } = this.state;

    return (
      <div className={classes.root}>
        <Paper square>
          <Tabs value={tabValue} centered
            onChange={this.on_TabChange.bind(this)}>
            <Tab label="Assets" />
            <Tab label="Playlists" />
          </Tabs>
        </Paper>
        {tabValue === 0 && <AssetTab assets={assets} loading={assetsLoading} />}
        {tabValue === 1 && <div>Not done...</div>}
      </div>
    );
  }
}

export default withStyles(styles)(AppTabs);
