import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

import Table from '@material-ui/core/Table';

import CircularProgress from '@material-ui/core/CircularProgress';

import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';

import PropTypes from 'prop-types';

/**
 * TableWithAddButton component.
 *
 * @class
 */
class TableWithAddButton extends Component {
  static propTypes = {
    children: PropTypes.node,
    classes: PropTypes.object.isRequired,
    loading: PropTypes.bool.isRequired,
    on_addButton_click: PropTypes.func.isRequired,
  };

  render() {
    const { classes, children, loading, on_addButton_click } = this.props;

    return (
      <div>
        {
          loading ?
            <div className={classes.center}>
              <CircularProgress className={classes.progress} />
            </div> :
            <Table>
              {children}
            </Table>
        }
        <Button variant="fab" color="primary" aria-label="Add"
          className={classes.floatingButton}
          onClick={on_addButton_click}
          disabled={loading}>
          <AddIcon />
        </Button>
      </div>
    );
  }
}

export default withStyles(theme => {
  return {
    floatingButton: {
      margin: theme.spacing.unit,
      position: 'absolute',
      bottom: theme.spacing.unit * 2,
      right: theme.spacing.unit * 2,
    },
    center: {
      display: 'flex',
      justifyContent: 'center',
    },
    progress: {
      marginTop: theme.spacing.unit * 5,
      marginRight: 'auto',
      marginLeft: 'auto',
    },
  };
})(TableWithAddButton);
