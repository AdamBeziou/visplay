import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
// import TextField from '@material-ui/core/TextField';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
// import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import PropTypes from 'prop-types';

/**
 * A list of all the assets.
 *
 * @class
 */
class AssetEdit extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
    on_close: PropTypes.func.isRequired,
  };

  render() {
    const { open, on_close } = this.props;

    const promptForClose = (...rest) => {
      on_close.call(on_close, ...rest);
    };

    return (
      <Dialog
        open={open}
        onClose={promptForClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Add Asset</DialogTitle>
        <DialogContent>
        </DialogContent>
        <DialogActions>
          <Button onClick={on_close} color="primary">
            Cancel
          </Button>
          <Button onClick={on_close} color="primary">
            Add
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default withStyles(() => {
  return {};
})(AssetEdit);
