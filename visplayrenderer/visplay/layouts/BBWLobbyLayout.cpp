/*
 *   This file is part of Visplay.
 *
 *   Visplay is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Visplay is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Visplay.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "visplay/layouts/BBWLobbyLayout.h"

namespace visplay::layouts 
{

BBWLayout::BBWLayout(QJsonObject& obj) 
{
    int font_size = 24;
    QString font_type = "Arial";

    layout = new QVBoxLayout();
    
    QPointer<QLabel> header = new QLabel();
    QPointer<QLabel> body_text = new QLabel();
    QFont font(font_type, font_size);

    header->setFixedHeight(font_size + 10 * (font_size / 12));
    header->setFont(font);
    body_text->setFont(font);

    header->setStyleSheet("QLabel { background-color : #C46342 ; color : white; }");
    body_text->setStyleSheet("QLabel { background-color : #3D4D7E ; color : white; }");

    body_text->setAlignment(Qt::AlignTop);
    header->setText(obj["header"].toString());
    body_text->setText(obj["body"].toString());
    
    body_text->setWordWrap(true);
    layout->addWidget(header);
    layout->addWidget(body_text);
     
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);

    qDebug() << "Creating BBWLobbyLayout";
}

BBWLayout::~BBWLayout()
{

}

void BBWLayout::display() 
{

}

}