/*
 *   This file is part of Visplay.
 *
 *   Visplay is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Visplay is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Visplay.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BBWLAYOUT_H
#define BBWLAYOUT_H

#include <QString>
#include <QLabel>
#include <QVBoxLayout>
#include <QDebug>
#include <QFont>
#include <QPointer>

#include "visplay/layouts/BaseLayout.h"

namespace visplay::layouts
{

class BBWLayout : public BaseLayout 
{
    Q_OBJECT

    public:
        BBWLayout(QJsonObject& obj);
        ~BBWLayout();

        void display();
};

}

#endif