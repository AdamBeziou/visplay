from visplay.sources import source_constructors as sc
import uri

import asyncio


async def run_sinks(session, sinks, asset):
    tvs = []
    for sink in sinks:
        path = uri.URI(sink)
        tvs.append(sc[str(path.scheme)].play(session, path, asset))
    await asyncio.gather(*tvs)
